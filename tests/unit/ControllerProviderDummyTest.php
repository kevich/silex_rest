<?php

class ControllerProviderDummyTest extends PHPUnit_Framework_TestCase
{
    public function testCreatesControllerDummyProviderObject()
    {
        $this->assertInstanceOf('ControllerProviderDummy', self::ControllerProviderDummy());
    }

    private static function ControllerProviderDummy($table = 'test')
    {
        return new ControllerProviderDummy($table);
    }
}
