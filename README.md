# Silex RESTful skeleton

This project helps you to create RESTful api to database from the box.

## Installation

    composer.phar install

## Config

Put information about database to config/default.json

ex.:

    "database.connection" : {
            "driver": "pdo_mysql",
            "dbname": "db",
            "user": "root",
            "password": "root",
            "charset": "utf8"
        }

## Default behavior

You can create folder, named like database table name, in app/Classes

`GET http://api.restsilex.com/tablename`
Will show list of all items in that table

`GET http://api.restsilex.com/tablename/id`
Will give you item with specified id

`POST http://api.restsilex.com/tablename/` with raw data `{"tablename":{"id":null,"name":"test"}}`
Will insert item with specified data and return json

