<?php

use Silex\ControllerCollection;

class ControllerProviderDummy extends ControllerProviderAbstract
{
    public function __construct($object_name) {
        $this->object_name = $object_name;
    }


    /**
     * @param ControllerCollection $controllers
     */
    protected function registerAdditionalControllers($controllers)
    {
    }
}
