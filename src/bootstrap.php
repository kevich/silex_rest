<?php

use Igorw\Silex\ConfigServiceProvider;
use Symfony\Component\HttpFoundation\Response;

define('ROOT_PATH',    __DIR__ . '/..');
define('APP_PATH',   ROOT_PATH . '/app');

require_once ROOT_PATH . '/vendor/autoload.php';

$app = new Silex\Application();

//loading default configuration
$app->register(new ConfigServiceProvider(APP_PATH . "/config/default.json"));

$env = getenv("APP_ENV") ? getenv("APP_ENV") : "dev";

//overriding configuration with environment specific
$app->register(new ConfigServiceProvider(APP_PATH . "/config/$env.json"));

//turn on error reporting for dev purpose
if ($env === "dev"){
    $app['debug'] = true;
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 1);
    ini_set('log_errors', 1);
}


include_once APP_PATH . '/app.php';

//create connection if configured
if ($app->offsetExists("database.connection")) {
    $app->register(new Silex\Provider\DoctrineServiceProvider(), array(
        "db.options" => $app["database.connection"],
    ));
}
//load routes files
$classesDir  = APP_PATH . "/Classes/";
$routes = scandir($classesDir);
$classes = array();

foreach ($routes as $dir){
    if (is_dir($classesDir . $dir)
        && $dir != '.'
        && $dir != '..') {
        $classes[] = $dir;
    }
}

$services = array();
foreach ($classes as $dir) {
    $serviceClass = $dir . '\Service';
    if (class_exists($serviceClass)) $services[strtolower($dir)]=$serviceClass;
}

$app->register(new ObjectServiceProvider($services));


foreach ($classes as $dir){
    $controllerClass = $dir . '\ControllerProvider';
    $app->mount("/".strtolower($dir),
            (class_exists($controllerClass) ? new $controllerClass : new ControllerProviderDummy(strtolower($dir))));
}

foreach ($app['controller.dummy-providers'] as $path => $table) {
    $app->mount($path, new ControllerProviderDummy($table));
}

$app->get('/', function () {
    return new Response("RESTful api service", 200);
});

return $app;